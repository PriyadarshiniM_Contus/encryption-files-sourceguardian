<?php
return [/*
* |--------------------------------------------------------------------------
* | Whitelisted Domains
* |--------------------------------------------------------------------------
* |
* | This file stores the whitelisted(authorized) domains to use the script
* |
*/
'whitelisted_domains' => [
    'localhost', 
    'api.vplayed.qa.contus.us', 
    'api.vplayed.dev.contus.us', 
    'api-vplayed-uat.contus.us',
    'api.eks.vplayed.com'
],
'toEmailAddresses' => ['balaganesh.g@contus.in ', 'arunkumar.r@contus.in']
];
